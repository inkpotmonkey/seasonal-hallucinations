import type { SeasonalKinds, SeasonalObject } from "./api/seasonal";
import { getLocalMonthNames } from "./utils";
import "./SeasonalityChart.css";
import { useAppContext } from "./Context";

const months = getLocalMonthNames().map((month) => <p key={month}>{month}</p>);

const createChartRow = (object: SeasonalObject, type: SeasonalKinds) =>
  Array.from({ length: 13 }).map((_, month) => {
    if (month === 0) {
      return (
        <p key={`${type}-${object.name}`} className={type}>
          {object.name}
        </p>
      );
    }

    return (
      <div
        className={object.inSeason.includes(month - 1) ? "inSeason" : ""}
        key={`${object.name}-${month - 1}`}
      ></div>
    );
  });

export default function SeasonalityChart() {
  const { selection, location, asking } = useAppContext();

  return (
    <div className="chart" data-asking={asking}>
      {months}
      {selection.length === 0 ? (
        <p>Why are you here?</p>
      ) : (
        selection.map((type) =>
          location.season[type].map((item) => createChartRow(item, type)),
        )
      )}
    </div>
  );
}
