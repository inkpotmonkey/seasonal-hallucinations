import {
  Dispatch,
  PropsWithChildren,
  SetStateAction,
  createContext,
  useContext,
  useState,
} from "react";

import { SeasonalKinds, allFoodKinds, seasonality } from "./api/seasonal";
import {
  Location,
  getTypedStorageItem,
  defaultLocation,
  Place,
  setTypedStorageItem,
} from "./data";

export interface TAppContext {
  selection: Array<SeasonalKinds>;
  setSelection: Dispatch<SetStateAction<Array<SeasonalKinds>>>;
  location: Location;
  updateLocation: (location?: Place) => Promise<void>;
  asking: boolean;
}

export const AppContext = createContext<TAppContext | undefined>(undefined);

const selectedTypes = getTypedStorageItem("selection") ?? allFoodKinds;
const savedLocation = getTypedStorageItem("location") ?? defaultLocation;

export const AppProvider = ({ children }: PropsWithChildren<unknown>) => {
  const [selection, setSelection] =
    useState<TAppContext["selection"]>(selectedTypes);
  const [location, setLocation] =
    useState<TAppContext["location"]>(savedLocation);
  const [asking, setAsking] = useState<TAppContext["asking"]>(false);

  const updateLocation = async (place = location.place) => {
    setAsking(true);

    const season = await seasonality(place);

    const updatedLocation = {
      place: { ...place },
      season: { ...season },
    };

    setLocation(updatedLocation);
    setTypedStorageItem("location", updatedLocation);
    setAsking(false);
  };

  return (
    <AppContext.Provider
      value={{ selection, setSelection, location, updateLocation, asking }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useAppContext = () => {
  const context = useContext(AppContext);

  if (!context) {
    throw new Error("useAppContext must be used inside the AppProvider");
  }

  return context;
};
