import { ChangeEvent, useState } from "react";

export default function Trust() {
  const [trust, setTrust] = useState(50);

  const handleRangeChange = (event: ChangeEvent<HTMLInputElement>) => {
    const changer = event.target.name;
    const value = Number.parseInt(event.target.value);

    switch (changer) {
      case "trust":
      case "aiTrust":
        setTrust(value);
        break;
      case "humanTrust":
        setTrust(Math.abs(value - 100));
        break;
    }
  };

  return (
    <div>
      <label htmlFor="trust">Trust</label>
      <div className="rangeContainer">
        <input
          type="range"
          id="trust"
          name="trust"
          min="0"
          max="100"
          value={trust}
          step="1"
          list="weights"
          onChange={(event) => handleRangeChange(event)}
        />
        <datalist id="weights">
          <option value="0" label="Human"></option>
          <option value="100" label="AI"></option>
        </datalist>
      </div>
      <div className="trustContainer">
        <input
          type="number"
          id="humanTrust"
          name="humanTrust"
          min="0"
          max="100"
          value={Math.abs(trust - 100)}
          onChange={(event) => handleRangeChange(event)}
        />
        <input
          type="number"
          id="aiTrust"
          name="aiTrust"
          min="0"
          max="100"
          value={trust}
          onChange={(event) => handleRangeChange(event)}
        />
      </div>
    </div>
  );
}
