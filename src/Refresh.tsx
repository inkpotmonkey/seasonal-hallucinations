import { useAppContext } from "./Context";

export default function Refresh() {
  const { updateLocation } = useAppContext();

  return (
    <button type="button" onClick={() => void updateLocation()}>
      Ask again
    </button>
  );
}
