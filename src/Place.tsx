import { ChangeEvent, useEffect, useRef, useState } from "react";
import { z } from "zod";
import { createZodFetcher } from "zod-fetch";

import type { Place } from "./data";
import { useAppContext } from "./Context";
import { logError } from "./utils";

const CountrySchema = z.object({
  name: z.string(),
  iso2: z.string(),
  iso3: z.string(),
  unicodeFlag: z.string(),
});

const CountriesResponseSchema = z.object({
  error: z.boolean(),
  msg: z.string(),
  data: z.array(CountrySchema),
});

type Countries = z.infer<typeof CountriesResponseSchema>;

const StateSchema = z.object({ name: z.string(), state_code: z.string() });

const StatesResponseSchema = z.object({
  error: z.boolean(),
  msg: z.string(),
  data: z.object({
    name: z.string(),
    iso3: z.string(),
    states: z.array(StateSchema),
  }),
});

type States = z.infer<typeof StatesResponseSchema>;

const fetchWithZod = createZodFetcher();

async function fetchCountries() {
  try {
    const countries = (
      await fetchWithZod(
        CountriesResponseSchema,
        "https://countriesnow.space/api/v0.1/countries/flag/unicode",
      )
    ).data.sort((a, b) => {
      if (a.name < b.name) {
        return -1;
      }
      if (a.name > b.name) {
        return 1;
      }
      return 0;
    });
    return countries;
  } catch (error) {
    logError(error, `Fetching countries failed with:`);
  }
}

// TODO: Change input on user selection to be a green button with a X to change the country or state
// TODO: Investigate whether it is possible to fine tune the html autocomplete so that
// a. the autocomplete is shown at the top of the list instead of the bottom (CSS?)
// b. it is possible to clear auto complete (this is required for the states input)
// https://developer.mozilla.org/en-US/docs/Web/HTML/Attributes/autocomplete
export default function Place() {
  const { location, updateLocation } = useAppContext();

  const [countries, setCountries] = useState<Countries["data"]>();
  const [states, setStates] = useState<States["data"]["states"]>([]);
  const [country, setCountry] = useState<string>(location?.place.country);

  useEffect(() => {
    let ignore = false;

    async function initialiseCountries() {
      const data = await fetchCountries();
      if (!ignore) {
        setCountries(data);
      }
    }

    void initialiseCountries();

    return () => {
      ignore = true;
    };
  }, []);

  const ref = useRef<HTMLInputElement>(null);

  useEffect(() => {
    async function runEffect() {
      if (!country) return;

      try {
        const res = await fetchWithZod(
          StatesResponseSchema,
          `https://countriesnow.space/api/v0.1/countries/states/q?country=${country}`,
        );
        setStates(res.data.states);
      } catch (error) {
        logError(error);
        //TODO: Show in the UI that no states were found
      }
    }
    void runEffect();
  }, [country]);

  const handleCountryInput = (event: ChangeEvent<HTMLInputElement>) => {
    const newCountry = event.target.value;
    const countryExists = countries?.some(
      (country) => country.name === newCountry,
    );

    if (countryExists) {
      setCountry(newCountry);
    }
  };

  const handleStateInput = (event: ChangeEvent<HTMLInputElement>) => {
    const state = event.target.value;
    const stateExists = states.some(
      (targetState) => targetState.name === state,
    );
    const countryExists = country !== undefined;

    if (!stateExists || !countryExists) {
      return;
    }

    const place: Place = {
      country,
      state,
    };

    void updateLocation(place);
  };

  return (
    <div>
      <label htmlFor="country">Hey AI whats in season in the country </label>
      <input
        id="country"
        name="country"
        required
        list="country-list"
        defaultValue={location.place.country}
        onChange={(event) => handleCountryInput(event)}
        autoComplete="off"
      />
      <datalist id="country-list">
        {countries?.map((country) => (
          <option key={country.name} value={country.name} />
        ))}
      </datalist>
      {country ? (
        <>
          <label htmlFor="state">in the state</label>
          <input
            ref={ref}
            id="state"
            name="state"
            required
            list="state-list"
            defaultValue={location.place.state}
            onChange={(event) => void handleStateInput(event)}
            autoComplete="off"
          />
          <datalist id="state-list">
            {states.map((state) => (
              <option key={state.name} value={state.name} />
            ))}
          </datalist>
        </>
      ) : undefined}
    </div>
  );
}
