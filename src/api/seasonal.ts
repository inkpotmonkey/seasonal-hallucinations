import Instructor from "@instructor-ai/instructor";
import OpenAI from "openai";
import { z } from "zod";

const oai = new OpenAI({
  apiKey: import.meta.env.VITE_OPENAI_API_KEY ?? undefined,
  organization: import.meta.env.VITE_OPENAI_ORG_ID ?? undefined,
  dangerouslyAllowBrowser: true,
});

const client = Instructor({
  client: oai,
  mode: "TOOLS",
});

const SeasonalObjectSchema = z
  .object({
    name: z
      .string()
      .describe(
        "The name of the fruit or vegetable always in singular form eg Strawberry",
      ),
    inSeason: z
      .number()
      .array()
      .max(12)
      .nonempty()
      .describe(
        "What months the fruit or vegetable is in season ie if in season in January, February and March this is [0,1,2]",
      ),
  })
  .describe(
    "An object that contains the name of a fruit or vegetable and the months it is in season eg {name: Strawberry, inSeason: [0,1,2]}",
  );

export type SeasonalObject = z.infer<typeof SeasonalObjectSchema>;

export const SeasonalSchema = z.object({
  // Description will be used in the prompt
  fruit: z
    .array(SeasonalObjectSchema)
    .describe(
      "List of fruits that are available in a certain location and what months they are in season.",
    ),
  vegetable: z
    .array(SeasonalObjectSchema)
    .describe(
      "List of vegetables that are available in a certain location and what months they are in season.",
    ),
});

export const allFoodKinds = SeasonalSchema.keyof().options;

export type Season = z.infer<typeof SeasonalSchema>;
export type SeasonalKinds = keyof Season;

export function isFoodKind(kind: string): kind is SeasonalKinds {
  return allFoodKinds.includes(kind as SeasonalKinds);
}

// Season will be of type z.infer<typeof SeasonalSchema>
export const seasonality = async <
  T extends { country: string; state?: string },
>(
  place: T,
) => {
  const messages: Array<OpenAI.Chat.Completions.ChatCompletionMessageParam> = [
    {
      role: "system",
      content:
        "You are an expert in world agriculture and are especially knowledgeable in what is in season around the world",
    },
    {
      role: "user",
      content: `Give me a complete list of fruits and vegetables and when they are in season that are available in the country ${place.country} and state ${place?.state}`,
    },
  ];

  const data = await client.chat.completions.create({
    messages,
    model: "gpt-3.5-turbo",
    response_model: {
      schema: SeasonalSchema,
      name: "User",
    },
  });

  console.log({ data });

  return data;
};
