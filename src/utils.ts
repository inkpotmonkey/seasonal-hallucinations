import ISO6391 from "iso-639-1";
import { getTypedStorageItem, setTypedStorageItem } from "./data";

/**
 * Check App for initial settings and if not create defaults
 */
export function initialise() {
  const lang = getTypedStorageItem("lang");
  if (!lang) {
    setTypedStorageItem("lang", navigator.language);
  }
}

/**
 * Return list of months formatted for the locale of the user
 */

// Refactor this function
export function getLocalMonthNames(
  locale?: Intl.LocalesArgument,
  monthFormat?: Intl.DateTimeFormatOptions["month"],
) {
  const options: Intl.DateTimeFormatOptions = {
    timeZone: "UTC",
    month: monthFormat ?? "short",
  } as const;

  const formatter = new Intl.DateTimeFormat(
    locale ?? navigator.language,
    options,
  );

  const months = Array.from({ length: 12 }).map((_, month) =>
    Date.UTC(0, month),
  );

  return months.map((month) => formatter.format(month));
}

/**
 * Change the users locale language
 */
export function changeLanguage(newLang: string) {
  const code = ISO6391.getCode(newLang);

  if (!ISO6391.validate(code)) {
    console.error(
      `Tried setting user locale to invalid native name: ${newLang}`,
    );
    return;
  }

  setTypedStorageItem("lang", code);
}

/* Error handling
 * https://kentcdodds.com/blog/get-a-catch-block-error-message-with-typescript
 */
interface ErrorWithMessage {
  message: string;
}

function isErrorWithMessage(error: unknown): error is ErrorWithMessage {
  return (
    typeof error === "object" &&
    error !== null &&
    "message" in error &&
    typeof (error as Record<string, unknown>).message === "string"
  );
}

function toErrorWithMessage(maybeError: unknown): ErrorWithMessage {
  if (isErrorWithMessage(maybeError)) return maybeError;

  try {
    return new Error(JSON.stringify(maybeError));
  } catch {
    // fallback in case there's an error stringifying the maybeError
    // like with circular references for example.
    return new Error(String(maybeError));
  }
}

function getErrorMessage(error: unknown) {
  return toErrorWithMessage(error).message;
}

export function logError(error: unknown, context?: string) {
  const message = getErrorMessage(error);
  console.error(`${context}${message}`);
}
