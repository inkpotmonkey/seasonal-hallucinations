import "@fontsource-variable/noto-serif";

import "./App.css";
import SeasonalityChart from "./SeasonalityChart";
import Filter from "./Filter";
import Place from "./Place";
import Trust from "./Trust";
import { AppProvider } from "./Context";
import Refresh from "./Refresh";

function App() {
  return (
    <AppProvider>
      <h1>Seasonal Hallucinations</h1>

      <Place />
      <div className="controls">
        <Trust />
        <div>
          <Refresh />
          <Filter />
        </div>
      </div>
      <SeasonalityChart />
    </AppProvider>
  );
}

export default App;
