import { ChangeEvent } from "react";

import { useAppContext } from "./Context";
import { allFoodKinds, isFoodKind } from "./api/seasonal";

export default function Filter() {
  const { selection, setSelection } = useAppContext();

  const handleFoodSelection = (event: ChangeEvent<HTMLInputElement>) => {
    const { checked, name } = event.target;
    if (!isFoodKind(name)) {
      throw new Error(`Checkbox received unknown food kind: ${name}`);
    }

    if (checked) {
      setSelection([...selection, name]);
    } else {
      setSelection(selection.filter((type) => type !== name));
    }
  };

  return (
    <ul className="typeList">
      {allFoodKinds.map((name) => (
        <li key={name}>
          <input
            type="checkbox"
            id={name}
            name={name}
            checked={selection.includes(name)}
            onChange={(event: ChangeEvent<HTMLInputElement>) =>
              handleFoodSelection(event)
            }
          />
          <label htmlFor={name}>{name}</label>
        </li>
      ))}
    </ul>
  );
}
