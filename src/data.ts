import type { Season, SeasonalKinds } from "./api/seasonal";

export interface Place {
  country: string;
  state?: string;
  unicodeFlag?: string;
}

interface Count {
  count: { AI?: number; human?: number };
}

export interface Location {
  place: Place;
  season: Season;
  count?: Count;
}

export const defaultLocation: Location = {
  place: { country: "", state: "" },
  season: {
    fruit: [],
    vegetable: [],
  },
};

interface Schema {
  lang: string;
  selection: Array<SeasonalKinds>;
  location: Location;
}

export const setTypedStorageItem = <T extends keyof Schema>(
  key: T,
  value: Schema[T],
): void => {
  const newValue = JSON.stringify(value);
  localStorage.setItem(key, newValue);

  const event = new StorageEvent("storage", {
    key,
    newValue,
  });

  window.dispatchEvent(event);
};

export const getTypedStorageItem = <T extends keyof Schema>(
  key: T,
): Schema[T] | null => {
  const item = localStorage.getItem(key);

  return item !== null ? (JSON.parse(item) as Schema[T]) : null;
};
