import { expect, test } from "bun:test";

import { getLocalMonthNames } from "../src/utils";

test("getLocalMonthNames", () => {
  const en = [
    "Jan",
    "Feb",
    "Mar",
    "Apr",
    "May",
    "Jun",
    "Jul",
    "Aug",
    "Sep",
    "Oct",
    "Nov",
    "Dec",
  ];

  const en_months = getLocalMonthNames();

  expect(en).toEqual(en_months);

  const hi = [
    "जन॰",
    "फ़र॰",
    "मार्च",
    "अप्रैल",
    "मई",
    "जून",
    "जुल॰",
    "अग॰,"
    "सित॰",
    "अक्तू॰",
    "नव॰",
    "दिस॰",
  ];

  const hi_months = getLocalMonthNames("hi");

  expect(hi).toEqual(hi_months);

  const ar = [
    "يناير",
    "فبراير",
    "مارس",
    "أبريل",
    "مايو",
    "يونيو",
    "يوليو",
    "أغسطس",
    "سبتمبر",
    "أكتوبر",
    "نوفمبر",
    "ديسمبر",
  ];

  const ar_months = getLocalMonthNames("ar");

  expect(ar).toEqual(ar_months);
});
