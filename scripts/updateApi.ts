const id = "fd652540-e179-11ee-bad8-8aad37b30b4b";
const token = Bun.argv[2];

const path = "./src/api/seasonal.ts";

const file = Bun.file(path);

let code = await file.text();

// Change import to match val.towns esm syntax
code = code.replace(/(from ")(.*)/g, "$1https://esm.sh/$2");

// Replace VITE for val.towns Deno import syntax
code = code.replace(/import.meta.env.VITE_(\w*)/g, 'Deno.env.get("$1")');

const method = "POST";
const headers = {
  "Content-Type": "application/json",
  Authorization: `Bearer ${token}`,
};
const body = JSON.stringify({ code });

try {
  let response = await fetch(`https://api.val.town/v1/vals/${id}/versions`, {
    method,
    headers,
    body,
  });

  if (!response.ok) {
    throw new Error(`HTTP error! response: ${JSON.stringify(response)}`);
  } else {
    let jsonResponse = await response.json();
    console.log(jsonResponse);
  }
} catch (error) {
  console.log(error);
}

export {};
